from django.contrib import admin

from rest.models import Item, PackList, PackItem, Category, TemplateItem, Destination, Duration, Person, Activity


class ItemInline(admin.StackedInline):
    model = PackItem
    extra = 5
    fields = ('item',)


class PackListAdmin(admin.ModelAdmin):
    inlines = [ItemInline]


admin.site.register(PackList, PackListAdmin)
admin.site.register(Item, admin.ModelAdmin)
admin.site.register(Category, admin.ModelAdmin)
admin.site.register(Person, admin.ModelAdmin)
admin.site.register(Duration, admin.ModelAdmin)
admin.site.register(Destination, admin.ModelAdmin)
admin.site.register(Activity, admin.ModelAdmin)
admin.site.register(TemplateItem, admin.ModelAdmin)
admin.site.register(PackItem, admin.ModelAdmin)
