"""frontend URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path, include
from rest_framework import routers
from rest_framework_simplejwt.views import TokenObtainPairView, TokenRefreshView, TokenVerifyView

# Routers provide an easy way of automatically determining the URL conf.

from rest.views import PackListViewSet, PackItemViewSet, ItemsViewSet, PersonViewSet, TemplateItemsViewSet, \
    DestinationViewSet, DurationViewSet, ActivityViewSet, ShareView

router = routers.DefaultRouter()
router.register('pack-lists', PackListViewSet)
router.register('pack-items', PackItemViewSet)
router.register('items', ItemsViewSet)
router.register('template-items', TemplateItemsViewSet)
router.register('persons', PersonViewSet)
router.register('durations', DurationViewSet)
router.register('destinations', DestinationViewSet)
router.register('activities', ActivityViewSet)
router.register('add-to-list', ShareView)

# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
urlpatterns = [
    path('', include(router.urls)),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    path('api/token/', TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('api/token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
    path('api/token/verify/', TokenVerifyView.as_view(), name='token_verify'),
]