import uuid
from abc import abstractmethod

from django.contrib.auth.models import User
from django.db import models


class Category(models.Model):
    name = models.CharField(max_length=100, unique=True)

    class Meta:
        verbose_name_plural = "Categories"


class Item(models.Model):
    name = models.CharField(max_length=50, unique=True)
    category = models.ForeignKey(Category, null=True, blank=True, on_delete=models.SET_NULL)

    def __str__(self):
        return self.name


class PackListTemplate(models.Model):
    name = models.CharField(max_length=30)
    items = models.ManyToManyField(Item)

    def __str__(self):
        return self.name


class PackList(models.Model):
    name = models.CharField(max_length=30)
    owners = models.ManyToManyField(User)
    share_id = models.UUIDField(default=uuid.uuid4, db_index=True)

    def __str__(self):
        return self.name


class PackItem(models.Model):
    item = models.ForeignKey(Item, on_delete=models.CASCADE)
    checked = models.BooleanField(default=False, null=False)
    list = models.ForeignKey(PackList, on_delete=models.CASCADE, related_name='items')

    class Meta:
        unique_together = [["item", "list"]]

    def __str__(self):
        return str(self.item)


class Type:
    PERSON = "PR"
    DESTINATION = "DS"
    DURATION = "DU"
    ACTIVITY = "AC"


class Template(models.Model):
    name = models.CharField(max_length=30)
    owners = models.ManyToManyField(User)
    order = models.IntegerField(default=0)

    class Meta:
        abstract = True

    @abstractmethod
    def type(self):
        pass

    def __str__(self):
        return self.name


class Person(Template):
    def type(self):
        return Type.PERSON


class Duration(Template):
    def type(self):
        return Type.DURATION


class Destination(Template):
    def type(self):
        return Type.DESTINATION


class Activity(Template):
    def type(self):
        return Type.ACTIVITY

    class Meta:
        verbose_name_plural = "Activities"


class TemplateItem(models.Model):
    persons = models.ManyToManyField(Person, blank=True)
    durations = models.ManyToManyField(Duration, blank=True)
    destinations = models.ManyToManyField(Destination, blank=True)
    activities = models.ManyToManyField(Activity, blank=True)
    owners = models.ManyToManyField(User)
    item = models.ForeignKey(Item, on_delete=models.CASCADE)

    def __str__(self):
        persons = ', '.join(str(template) for template in self.persons.all())
        durations = ', '.join(str(template) for template in self.durations.all())
        destinations = ', '.join(str(template) for template in self.destinations.all())
        activities = ', '.join(str(template) for template in self.activities.all())
        return "{} {} {} {}: {}".format(persons, durations, destinations, activities, self.item)
