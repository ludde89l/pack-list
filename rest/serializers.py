from rest_framework import serializers

from rest.models import Item, PackList, PackItem, Template, TemplateItem, Person, Activity, Destination, Duration


class PackItemSerializer(serializers.ModelSerializer):

    list_id = serializers.IntegerField(write_only=True, required=False)

    class Meta:
        model = PackItem
        fields = ['id', 'item', 'checked', 'list_id']


class PersonSerializer(serializers.ModelSerializer):

    class Meta:
        model = Person
        fields = ['id', 'name']


class DestinationSerializer(serializers.ModelSerializer):

    class Meta:
        model = Destination
        fields = ['id', 'name']


class ActivitySerializer(serializers.ModelSerializer):

    class Meta:
        model = Activity
        fields = ['id', 'name']


class DurationSerializer(serializers.ModelSerializer):

    class Meta:
        model = Duration
        fields = ['id', 'name', 'order']


class TemplateItemSerializer(serializers.ModelSerializer):

    class Meta:
        model = TemplateItem
        fields = ['id', 'item', 'persons', 'durations', 'destinations', 'activities']


class PackListSerializer(serializers.HyperlinkedModelSerializer):

    items = PackItemSerializer(many=True)
    shareId = serializers.CharField(source="share_id", read_only=True)

    def create(self, validated_data):
        items_data = validated_data.pop('items')
        list = PackList.objects.create(**validated_data)
        for item_data in items_data:
            PackItem.objects.create(**item_data, list=list)
        return list

    class Meta:
        model = PackList
        fields = ['id', 'name', 'items', 'shareId']


class ItemSerializer(serializers.HyperlinkedModelSerializer):

    category = serializers.CharField(source="category.name", default=None)

    class Meta:
        model = Item
        fields = ['id', 'name', 'category']
