from django.db.models import Q
from rest_framework import viewsets, mixins
from rest_framework.exceptions import APIException
from rest_framework.permissions import IsAuthenticated, BasePermission, SAFE_METHODS
from rest_framework.response import Response
from rest_framework.status import HTTP_409_CONFLICT
from rest_framework.viewsets import GenericViewSet

from rest.models import Item, PackItem, PackList, Template, TemplateItem, Person, Duration, Destination, Activity
from rest.serializers import ItemSerializer, PackListSerializer, PackItemSerializer, PersonSerializer, \
    TemplateItemSerializer, DestinationSerializer, DurationSerializer, ActivitySerializer


class PackListViewSet(viewsets.ModelViewSet):

    def perform_create(self, serializer):
        pack_list = serializer.save()
        pack_list.owners.set([self.request.user])
        return pack_list

    def get_queryset(self):
        user = self.request.user
        return self.queryset.filter(owners=user)

    class PackListPermission(IsAuthenticated):

        def has_object_permission(self, request, view, obj: PackList):
            return request.user in obj.owners.all()

    permission_classes = [PackListPermission]
    queryset = PackList.objects.all()
    serializer_class = PackListSerializer


class PackItemViewSet(viewsets.ModelViewSet):

    def get_queryset(self):
        user = self.request.user
        return self.queryset.filter(list__owners=user)

    class ItemPermission(IsAuthenticated):

        def has_object_permission(self, request, view, obj: PackItem):
            return request.user in obj.list.owners.all()

    permission_classes = [ItemPermission]
    queryset = PackItem.objects.all()
    serializer_class = PackItemSerializer


class TemplateViewSet(viewsets.ModelViewSet):
    class TemplatePermission(IsAuthenticated):

        def has_object_permission(self, request, view, obj: Template):
            return request.user in obj.owners.all()

    def get_queryset(self):
        user = self.request.user
        return self.queryset.filter(owners=user)

    def perform_create(self, serializer):
        person = serializer.save()
        person.owners.set([self.request.user])
        return person

    permission_classes = [TemplatePermission]


class PersonViewSet(TemplateViewSet):
    queryset = Person.objects.all()
    serializer_class = PersonSerializer


class DurationViewSet(TemplateViewSet):
    queryset = Duration.objects.all()
    serializer_class = DurationSerializer


class DestinationViewSet(TemplateViewSet):
    queryset = Destination.objects.all()
    serializer_class = DestinationSerializer


class ActivityViewSet(TemplateViewSet):
    queryset = Activity.objects.all()
    serializer_class = ActivitySerializer


class TemplateItemsViewSet(viewsets.ModelViewSet):

    def get_queryset(self):
        queryset = TemplateItem.objects.all()
        persons = self.request.query_params.getlist('person', None)
        if persons:
            queryset = queryset.filter(Q(persons__in=persons) | Q(persons=None))
        durations = self.request.query_params.getlist('duration', None)
        if durations:
            queryset = queryset.filter(Q(durations__in=durations) | Q(durations=None))
        destinations = self.request.query_params.getlist('destination', None)
        if destinations:
            queryset = queryset.filter(Q(durations__in=destinations) | Q(durations=None))
        activities = self.request.query_params.getlist('activity', None)
        if activities:
            queryset = queryset.filter(Q(activities__in=activities) | Q(activities=None))
        return queryset

    queryset = TemplateItem.objects.all()
    serializer_class = TemplateItemSerializer


class ItemsViewSet(viewsets.ModelViewSet):
    class IsAdminOrReadOnly(BasePermission):

        def has_permission(self, request, view):
            return bool(
                request.method in SAFE_METHODS or
                request.user and
                request.user.is_authenticated and
                request.user.is_staff
            )

    permission_classes = [IsAdminOrReadOnly]
    queryset = Item.objects.all()
    serializer_class = ItemSerializer


class ShareView(mixins.UpdateModelMixin, GenericViewSet):

    def perform_update(self, serializer: PackListSerializer):
        pack_list = serializer.save()
        if self.request.user in pack_list.owners.all():
            raise ConflictException(detail=serializer.data)
        pack_list.owners.add(self.request.user)
        return pack_list

    permission_classes = [IsAuthenticated]
    lookup_field = 'share_id'
    queryset = PackList.objects.all()
    serializer_class = PackListSerializer


class ConflictException(APIException):
    status_code = 409
